import path from 'path';
import level, { LevelDB } from 'level';
import { getKeys, findKey, getValues, findKeys, sanitize, hasKey } from './db';
import chalk from 'chalk';

function showHelp() {
    console.log(`
Usage:

   ola leveldb=<leveldb folder pathname> 
       [script=<javascript file> script=<javascript file> ...]

"leveldb" option is mandatory. Each additional test script is added with a 
"script" option.
`);
}

function getCurrentPersonID(db: LevelDB): Promise<string> {
    return new Promise((resolve, reject) => {
        findKey(db, /authtoken$/).then(
            (key) => {
                if ((key as string).length > 0) {
                    db.get(key, (error, value) => {
                        var authtoken = JSON.parse(sanitize(value));
                        resolve(authtoken.id);
                    })
                } else {
                    resolve('');
                }
            }
        ).catch(
            (error) => { reject(error); }
        );

    })
}

function _checkMissing(db: LevelDB, key: string, re: (so: string) => RegExp): Promise<{ key: string, missing: string[] }> {
    return new Promise((resolve, reject) => {
        getKeys(db).then((allKeys) => {
            db.get(key, (error, value) => {
                if (error) {
                    reject(error);
                } else {
                    // Get a list of SOs from a given key
                    if (sanitize(value).length > 0) {
                        // var missingSOs: string[] = [];
                        var list = JSON.parse(sanitize(value));

                        var subset = (list as string[]).filter((so) => {
                            return ((allKeys as string[]).findIndex((key) => {
                                return re(so).test(key);
                            }) < 0)
                        })

                        resolve({ key: key, missing: subset });
                    } else {
                        resolve({ key: key, missing: [] });
                    }

                }
            })
        }).catch((error) => reject(error));
    })
}

function checkMissingSO(db: LevelDB, key: string): Promise<{ key: string, missing: string[] }> {
    return _checkMissing(db, key, (so: string) => new RegExp(`${so}\.so$`));
}

function checkMissingRpt(db: LevelDB, key: string): Promise<{ key: string, missing: string[] }> {
    return _checkMissing(db, key, (so: string) => new RegExp(`${so}\.rpt$`));
}

function checkMissingSOAndRpt(db: LevelDB, key: string): Promise<{ key: string, missing: string[] }> {
    return _checkMissing(db, key, (so: string) => new RegExp(`${so}\.(so|rpt)$`));
}

function removeSO(db: LevelDB, key: string, serviceOrders: string[]): Promise<void> {
    return new Promise((resolve, reject) => {
        db.get(key).then((value) => {
            var list = JSON.parse(sanitize(value));

            serviceOrders.forEach((so) => {
                console.log(`Remove ${so}`)
                list.splice(list.indexOf(so), 1);
            });

            console.log(`Update ${key}`);
            db.put(key, `\u0001${JSON.stringify(list)}`).then(() => {
                console.log(chalk.green("Successful"));
                resolve();
            }).catch((error) => {
                reject(error);
            });
        }).catch((error) => {
            reject(error);
        });

    })
}


/*
 * Usage:
 *
 * node dist/main.js leveldb="/Users/edwardyee/Library/Application Support/OnSight-Local/Local Storage/leveldb"
 * 
 * With additional test scripts:
 * 
 * node dist/main.js leveldb="/Users/edwardyee/Library/Application Support/OnSight-Local/Local Storage/leveldb"
 *                   script="./test1.js" script="./test2.js"
 */

// process.argv.forEach((val, idx) => {
//     console.log(`${idx}: ${val}`)
// })
// console.log(__dirname)

// Extract leveldb pathname from command line arguments
var ldbp = process.argv.find((val) => {
    return val.startsWith("leveldb");
})

var localdbPath: string;
if (ldbp) {
    var [_, localdbPath] = ldbp.split('=');
    console.log(localdbPath);
} else {
    // console.log("Missing localdb option")
    showHelp();
    process.exit(1);
}

// Open leveldb
var db = level(localdbPath, { keyEncoding: 'utf8', valueEncoding: 'utf8' });

// Test PUT()
// db.put('testKey.scheduled', JSON.stringify({ a: 'b', b: 2, c: true }));
// db.put('testKey.scheduled', JSON.stringify({ a: 'b', c: true }));
// db.del('testKey.scheduled');

// // Go through leveldb and extract certain value
// getKeys(db).then((keys) => {
//     // console.log("Results")
//     // console.log('>>', JSON.stringify(keys))

//     (keys as string[]).forEach((key) => {
//         if (/\.(pending|scheduled|removed|submitted)$/.test(key)) {
//             db.get(key, (error, value) => {
//                 if (error) {
//                     console.error(error);
//                 } else {
//                     console.log(`${key} => ${JSON.stringify(value)}`)
//                 }

//             })
//         }
//     })

// })

// getValues(db).then((values) => {
//     console.log(values);
// })

getCurrentPersonID(db).then(
    (id) => {

        if ((id as string).length > 0) {
            // Has current user
            var re = new RegExp(`${id}\.(pending|scheduled|removed|submitted)$`);
        } else {
            // No current user
            var re = new RegExp(`\.(pending|scheduled|removed|submitted)$`);
        }

        findKeys(db, re).then((keys) => {
            Promise.all(
                (keys as string[]).sort().map((key) => {
                    if (key.indexOf('pending') >= 0) {
                        return checkMissingRpt(db, key);
                    } else if (key.indexOf('scheduled') >= 0) {
                        return checkMissingSO(db, key);
                    } else if (key.indexOf('removed') >= 0) {
                        return checkMissingSO(db, key);
                    } else if (key.indexOf('submitted') >= 0) {
                        return checkMissingRpt(db, key);
                    }
                })
            ).then((results) => {
                results.forEach((result) => {
                    if (result) {
                        if (result.missing.length > 0) {
                            console.log(chalk.redBright(`${result.key} has bad SOs: ${JSON.stringify(result.missing)}`));

                            db.createReadStream({
                                gte: result.key,
                                lte: result.key
                            }).on('data', (value) => {
                                console.log(value);
                            })

                            removeSO(db, result.key, result.missing).then(() => {
                                db.createReadStream({
                                    gte: result.key,
                                    lte: result.key
                                }).on('data', (value) => {
                                    console.log(value);
                                })

                            }).catch((error) => {
                                console.error(error);
                            });


                        } else {
                            console.log(`${result.key} is good`);
                        }

                    }
                })
            })
        });
    }
).catch(
    (error) => {
        console.error(error);
        process.exit(2);
    }
);

// Trying to get additional scripts from the command line arguments
var scripts = process.argv.filter((val) => {
    return val.startsWith('script=');
})

// Execute the test(db) method in each script
if (scripts.length > 0) {
    scripts.forEach((script) => {
        var [_, file] = script.split('=');

        import(__dirname + path.sep + file).then((module) => module.test(db)).catch((error) => console.error(error));
    })
}

// db.createReadStream().on('data', (value) => {
//     console.log(value);
// })
