# OnSight LS Analyser

This project demonstrates the ability to access leveldb file.

# Initial Setup

The following information is meant for setting up a new Typescript enabled NodeJS project

1. Install Typescript
1. Create `ts.config` file
1. Add `rimraf, concurrently, and nodemon`

| Command                                | Description                                                              |
| -------------------------------------- | ------------------------------------------------------------------------ |
| `npm i -D typescript`                  | Install Typescript locally.                                              |
| `tsc --init`                           | Create a base `ts.config` file                                           |
| `npm i -D rimraf concurrently nodemon` | These modules allow currently monitor and compile changes in the project |

## References:

[A TypeScript Project Structure Guide](https://javascript.plainenglish.io/typescript-project-directory-structure-module-resolution-and-related-configuration-options-1d8b87ffec88)

[Tips for setting up a TypeScript nodejs project](https://codeburst.io/tips-for-setting-up-a-typescript-nodejs-project-5d1c48dc1a2d)

[The Ultimate Guide to setting up Node.js with TypeScript and Express](https://ultimatecourses.com/blog/setup-typescript-nodejs-express)
