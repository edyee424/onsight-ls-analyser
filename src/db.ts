import { LevelDB } from "level";

/**
 * Sanitize LevelDB value
 * @param src Source string
 * @returns Trimmed value
 */
export function sanitize(src: string) {
    return src.replace(/^\u0001/, '');
}

/**
 * Get all LevelDB keys
 * @param db LevelDB instance
 * @returns List of all LevelDB keys
 */
export function getKeys(db: LevelDB) {
    var allKeys: string[] = [];
    return new Promise((resolve, reject) => {
        var stream = db.createKeyStream();

        stream.on('data', (key) => { allKeys.push(sanitize(key)) });
        stream.on('close', () => { resolve(allKeys) });
    })
}

/**
 * Get all LevelDB values
 * @param db LevelDB instance
 * @returns List of all LevelDB values
 */
export function getValues(db: LevelDB) {
    var allValues: string[] = [];
    return new Promise((resolve, reject) => {
        var stream = db.createValueStream();

        stream.on('data', (value) => { allValues.push(sanitize(value)) });
        stream.on('close', () => resolve(allValues));
    })
}

/**
 * Find a key by regular expression pattern
 * @param db LevelDB instance
 * @param pattern Regular expression pattern
 * @returns The first key that matches the pattern. Otherwise, it will return an empty string
 */
export function findKey(db: LevelDB, pattern: RegExp) {
    return new Promise((resolve, reject) => {
        getKeys(db).then((keys) => {
            var matchedKey = (keys as string[]).find((key) => pattern.test(key));

            if (matchedKey) {
                resolve(sanitize(matchedKey));
            } else {
                resolve('');
            }
        }).catch((error) => {
            reject(error);
        })
    })
}

/**
 * Get a list of keys by regular expression pattern
 * @param db LevelDB instance
 * @param pattern Regular expression pattern
 * @returns List of matched keys
 */
export function findKeys(db: LevelDB, pattern: RegExp) {
    var allKeys: string[] = [];
    return new Promise((resolve, reject) => {
        getKeys(db).then((keys) => {
            allKeys = (keys as string[]).filter((key) => pattern.test(key)).map((key) => sanitize(key));
            resolve(allKeys);
        }).catch((error) => {
            reject(error);
        })
    })

}

/**
 * Determine the given key existed or not
 * @param db LevelDB instance
 * @param key Key
 * @returns TRUE - existed
 */
export function hasKey(db: LevelDB, key: string) {
    return new Promise((resolve, reject) => {
        db.get(key, (error, value) => {
            if (error) {
                resolve(false);
            } else {
                resolve(true)
            }
        })
    })
}